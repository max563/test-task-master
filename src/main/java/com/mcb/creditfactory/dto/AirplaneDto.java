package com.mcb.creditfactory.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonTypeName("airplane")
public class AirplaneDto implements Collateral {
    private Long id;
    private String brand;
    private String model;
    private String manufacturer;
    private Short year;
    @JsonManagedReference
    private Set<AirplaneValuationDto> airplaneValuations=new HashSet<>();
    private int fuelCapacity;
    private int seats;

    public void addValuationDto(AirplaneValuationDto airplaneValuationDto){
        airplaneValuations.add(airplaneValuationDto);
        airplaneValuationDto.setAirplaneDto(this);
    }

    public void removeValuationDto(AirplaneValuationDto airplaneValuationDto){
        airplaneValuations.remove(airplaneValuationDto);
        airplaneValuationDto.setAirplaneDto(null);
    }
}

