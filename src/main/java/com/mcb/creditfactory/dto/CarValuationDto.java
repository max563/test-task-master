package com.mcb.creditfactory.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarValuationDto implements Comparable<CarValuationDto> {
    private Long id;
    private LocalDate date;
    private BigDecimal value;

    public CarValuationDto(Long id, LocalDate date, BigDecimal value){
        this.id=id; this.date=date; this.value=value;
    }

    @JsonBackReference
    private CarDto carDto;

    @Override
    public String toString() {
        return "CarValuationDto{" +
                "id=" + id +
                ", date=" + date +
                ", value=" + value +
                '}';
    }

    @Override
    public int compareTo(CarValuationDto o) {
        return date.compareTo(o.getDate());
    }
}
