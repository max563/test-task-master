package com.mcb.creditfactory.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonTypeName("car")
public class CarDto implements Collateral {
    private Long id;
    private String brand;
    private String model;
    private Double power;
    private Short year;
    @JsonManagedReference
    private Set<CarValuationDto> carValuations=new HashSet<>();

    public void addValuationDto(CarValuationDto carValuationDto){
        carValuations.add(carValuationDto);
        carValuationDto.setCarDto(this);
    }

    public void removeValuationDto(CarValuationDto carValuationDto){
        carValuations.remove(carValuationDto);
        carValuationDto.setCarDto(null);
    }
}
