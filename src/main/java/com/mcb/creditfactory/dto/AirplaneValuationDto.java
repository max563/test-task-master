package com.mcb.creditfactory.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AirplaneValuationDto implements Comparable<AirplaneValuationDto>{
    private Long id;
    private LocalDate date;
    private BigDecimal value;

    public AirplaneValuationDto(Long id, LocalDate date, BigDecimal value){
        this.id=id; this.date=date; this.value=value;
    }

    @JsonBackReference
    private AirplaneDto airplaneDto;

    @Override
    public String toString() {
        return "AirplaneValuationDto{" +
                "id=" + id +
                ", date=" + date +
                ", value=" + value +
                '}';
    }

    @Override
    public int compareTo(AirplaneValuationDto o) {
        return date.compareTo(o.getDate());
    }
}
