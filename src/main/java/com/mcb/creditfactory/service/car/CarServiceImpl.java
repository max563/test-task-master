package com.mcb.creditfactory.service.car;

import com.mcb.creditfactory.dto.CarDto;
import com.mcb.creditfactory.dto.CarValuationDto;
import com.mcb.creditfactory.external.ExternalApproveService;
import com.mcb.creditfactory.model.Car;
import com.mcb.creditfactory.model.CarValuation;
import com.mcb.creditfactory.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CarServiceImpl implements CarService {
    @Autowired
    private ExternalApproveService approveService;

    @Autowired
    private CarRepository carRepository;

    @Override
    public boolean approve(CarDto dto) {
        return approveService.approve(new CarAdapter(dto)) == 0;
    }

    @Override
    public Car save(Car car) {
        return carRepository.save(car);
    }

    @Override
    public Optional<Car> load(Long id) {
        return carRepository.findById(id);
    }

    @Override
    public Car fromDto(CarDto dto) {
        Car car=new Car();
        Set<CarValuationDto> carValuationDtos = dto.getCarValuations();
        if (carValuationDtos != null) {
            for (CarValuationDto carValuationDto : carValuationDtos) {
                car.addValuation(new CarValuation(
                        carValuationDto.getDate(),
                        carValuationDto.getValue()));
            }
        }

        car.setId(dto.getId());
        car.setBrand(dto.getBrand());
        car.setModel(dto.getModel());
        car.setPower(dto.getPower());
        car.setYear(dto.getYear());

        return car;
    }

    @Override
    public CarDto toDTO(Car car) {
        CarDto carDto=new CarDto();
        Set<CarValuation> carValuations = car.getCarValuations();
        for (CarValuation carValuation : carValuations) {
            carDto.addValuationDto(new CarValuationDto(
                    carValuation.getId(),
                    carValuation.getDate(),
                    carValuation.getValue()));
        }
        carDto.setId(car.getId());
        carDto.setBrand(car.getBrand());
        carDto.setModel(car.getModel());
        carDto.setPower(car.getPower());
        carDto.setYear(car.getYear());
        return carDto;
    }

    @Override
    public Long getId(Car car) {
        return car.getId();
    }
}
