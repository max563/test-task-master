package com.mcb.creditfactory.service.car;

import com.mcb.creditfactory.dto.CarDto;
import com.mcb.creditfactory.dto.CarValuationDto;
import com.mcb.creditfactory.external.CollateralObject;
import com.mcb.creditfactory.external.CollateralType;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;
import java.util.TreeSet;

@AllArgsConstructor
public class CarAdapter implements CollateralObject {
    private CarDto car;

    @Override
    public BigDecimal getValue() {
        return getCarValuationDto().getValue();
    }

    @Override
    public Short getYear() {
        return car.getYear();
    }

    @Override
    public LocalDate getDate() {
        return getCarValuationDto().getDate();
    }

    @Override
    public CollateralType getType() {
        return CollateralType.CAR;
    }

    private CarValuationDto getCarValuationDto(){
        Set<CarValuationDto> temp= car.getCarValuations();
        TreeSet<CarValuationDto> carValuationDtos = new TreeSet<>(temp);
        CarValuationDto carValuationDto=carValuationDtos.last();
        return carValuationDto;
    }
}
