package com.mcb.creditfactory.service.airplane;

import com.mcb.creditfactory.dto.AirplaneDto;
import com.mcb.creditfactory.dto.AirplaneValuationDto;
import com.mcb.creditfactory.external.CollateralObject;
import com.mcb.creditfactory.external.CollateralType;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;
import java.util.TreeSet;

@AllArgsConstructor
public class AirplaneAdapter implements CollateralObject {
    private AirplaneDto airplane;

    @Override
    public BigDecimal getValue() {
        return getAirplaneValuationDto().getValue();
    }

    @Override
    public Short getYear() {
        return airplane.getYear();
    }

    @Override
    public LocalDate getDate() {
        return getAirplaneValuationDto().getDate();
    }

    @Override
    public CollateralType getType() {
        return CollateralType.AIRPLANE;
    }

    private AirplaneValuationDto getAirplaneValuationDto(){
        Set<AirplaneValuationDto> temp=airplane.getAirplaneValuations();
        TreeSet<AirplaneValuationDto> airplaneValuationDtos = new TreeSet<>(temp);
        AirplaneValuationDto airplaneValuationDto=airplaneValuationDtos.last();
        return airplaneValuationDto;
    }
}
