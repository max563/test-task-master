package com.mcb.creditfactory.service.airplane;

import com.mcb.creditfactory.dto.AirplaneDto;
import com.mcb.creditfactory.dto.AirplaneValuationDto;
import com.mcb.creditfactory.external.ExternalApproveService;
import com.mcb.creditfactory.model.Airplane;
import com.mcb.creditfactory.model.AirplaneValuation;
import com.mcb.creditfactory.repository.AirplaneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class AirplaneServiceImpl implements AirplaneService {
    @Autowired
    private ExternalApproveService approveService;

    @Autowired
    private AirplaneRepository airplaneRepository;

    @Override
    public boolean approve(AirplaneDto dto) {
        return approveService.approve(new AirplaneAdapter(dto)) == 0;
    }

    @Override
    public Airplane save(Airplane airplane) {
        return airplaneRepository.save(airplane);
    }

    @Override
    public Optional<Airplane> load(Long id) {
        return airplaneRepository.findById(id);
    }

    @Override
    public Airplane fromDto(AirplaneDto dto) {
        Airplane airplane=new Airplane();
        Set<AirplaneValuationDto> airplaneValuationDtos = dto.getAirplaneValuations();
        if (airplaneValuationDtos != null) {
            for (AirplaneValuationDto airplaneValuationDto : airplaneValuationDtos) {

                airplane.addValuation(new AirplaneValuation(
                        airplaneValuationDto.getDate(),
                        airplaneValuationDto.getValue()));
            }
        }

        airplane.setId(dto.getId());
        airplane.setBrand(dto.getBrand());
        airplane.setModel(dto.getModel());
        airplane.setManufacturer( dto.getManufacturer());
        airplane.setYear(dto.getYear());
        airplane.setFuelCapacity(dto.getFuelCapacity());
        airplane.setSeats(dto.getSeats());

        return airplane;
    }

    @Override
    public AirplaneDto toDTO(Airplane airplane) {
        AirplaneDto airplaneDto=new AirplaneDto();
        Set<AirplaneValuation> airplaneValuations = airplane.getAirplaneValuations();
        for (AirplaneValuation airplaneValuation : airplaneValuations) {
            airplaneDto.addValuationDto(new AirplaneValuationDto(
                    airplaneValuation.getId(),
                    airplaneValuation.getDate(),
                    airplaneValuation.getValue()));

        }

        airplaneDto.setId(airplane.getId());
        airplaneDto.setBrand(airplane.getBrand());
        airplaneDto.setModel(airplane.getModel());
        airplaneDto.setManufacturer(airplane.getManufacturer());
        airplaneDto.setYear(airplane.getYear());
        airplaneDto.setFuelCapacity(airplane.getFuelCapacity());
        airplaneDto.setSeats(airplane.getSeats());

        return airplaneDto;
    }

    @Override
    public Long getId(Airplane airplane) {
        return airplane.getId();
    }
}
