package com.mcb.creditfactory.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "AIRPLANE")
public class Airplane {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String brand;
    private String model;
    private String manufacturer;
    @Column(name = "year_of_issue")
    private Short year;

    @OneToMany(mappedBy = "airplane", cascade=CascadeType.ALL)
    private Set<AirplaneValuation> airplaneValuations=new HashSet<>();
    @Column(name = "fuel_capacity")
    private int fuelCapacity;
    private int seats;

    public void addValuation(AirplaneValuation airplaneValuation){
        airplaneValuations.add(airplaneValuation);
        airplaneValuation.setAirplane(this);
    }
    public void removeValuation(AirplaneValuation airplaneValuation){
        airplaneValuations.remove(airplaneValuation);
        airplaneValuation.setAirplane(null);
    }


}
