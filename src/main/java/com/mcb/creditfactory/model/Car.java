package com.mcb.creditfactory.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "CAR")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String brand;
    private String model;
    private Double power;
    @Column(name = "year_of_issue")
    private Short year;

    @OneToMany(mappedBy = "car", cascade=CascadeType.ALL)
    private Set<CarValuation> carValuations=new HashSet<>();

    public void addValuation(CarValuation carValuation){
        carValuations.add(carValuation);
        carValuation.setCar(this);
    }
    public void removeValuation(CarValuation carValuation){
        carValuations.add(carValuation);
        carValuation.setCar(null);
    }
}
