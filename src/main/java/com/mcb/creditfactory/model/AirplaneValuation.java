package com.mcb.creditfactory.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AIRPLANE_VALUATION")
public class AirplaneValuation implements Comparable<AirplaneValuation>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate date;
    private BigDecimal value;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "airplane_id")
    private Airplane airplane;

    public AirplaneValuation(LocalDate date, BigDecimal value){
        this.date=date;
        this.value=value;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof AirplaneValuation )) return false;
        return id != null && id.equals(((AirplaneValuation) o).getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }

    //без этого, в дебагере получаем stackoverflow, тут исключен объект airplane чтобы небыло рекурсии
    @Override
    public String toString() {
        return "AirplaneValuation{" +
                "id=" + id +
                ", date=" + date +
                ", value=" + value +
                '}';
    }

    @Override
    public int compareTo(AirplaneValuation o) {
        return date.compareTo(o.getDate());
    }
}