package com.mcb.creditfactory.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CAR_VALUATION")
public class CarValuation implements Comparable<CarValuation> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate date;
    private BigDecimal value;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_id")
    private Car car;

    public CarValuation(LocalDate date, BigDecimal value){
        this.date=date;
        this.value=value;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof AirplaneValuation )) return false;
        return id != null && id.equals(((CarValuation) o).getId());
    }

    @Override
    public int hashCode() {
        return 41;
    }

    //без этого, в дебагере получаем stackoverflow, тут исключен объект car чтобы небыло рекурсии
    @Override
    public String toString() {
        return "CarValuation{" +
                "id=" + id +
                ", date=" + date +
                ", value=" + value +
                '}';
    }

    @Override
    public int compareTo(CarValuation o) {
        return date.compareTo(o.getDate());
    }
}
