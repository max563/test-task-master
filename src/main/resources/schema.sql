create table CAR (
  id IDENTITY primary key,
  brand VARCHAR2(150),
  model VARCHAR2(200),
  power DOUBLE,
  year_of_issue YEAR,
);

create table AIRPLANE (
  id IDENTITY primary key,
  brand VARCHAR2(150),
  model VARCHAR2(200),
  manufacturer VARCHAR2(500),
  year_of_issue YEAR,
  fuel_capacity INT,
  seats INT
);

create table CAR_VALUATION(
 id IDENTITY primary key,
 date DATE,
 value DEC(20),
 car_id INT,
 FOREIGN KEY (car_id) REFERENCES CAR (id)
);

create table AIRPLANE_VALUATION(
 id IDENTITY primary key,
 date DATE,
 value DEC(20),
 airplane_id INT,
 FOREIGN KEY (airplane_id) REFERENCES AIRPLANE (id)
);