package com.mcb.creditfactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcb.creditfactory.dto.*;
import com.mcb.creditfactory.model.CarValuation;
import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestTaskApplicationTests {

    @Test
    public void contextLoads() {
//        String resourceURL = "http://localhost:8080/collateral/save";
//        RestTemplate restTemplate=new RestTemplate();
//        HttpEntity<Collateral> request = new HttpEntity<>(new CarDto(1L,"lada","2110",
//                86D, (short) 2005,new BigDecimal("1100000")));
//        Long response = restTemplate.postForObject(resourceURL,request, Long.class);
//        System.out.println(response);
//        assertThat(response, containsString(""));
    }

    @Test
    public void testSave(){
        String resourceURL = "http://localhost:8080/collateral/save";
        RestTemplate restTemplate=new RestTemplate();
//        HttpEntity<Collateral> request = new HttpEntity<>(getTestCarDto());
        HttpEntity<Collateral> request = new HttpEntity<>(getTestAirplaneDto());
        Long response = restTemplate.postForObject(resourceURL,request, Long.class);
        System.out.println(response);
        assertTrue(response>0);
    }

    @Test
    public void testGetInfo(){
        String resourceURL = "http://localhost:8080/collateral/info";
        RestTemplate restTemplate=new RestTemplate();
//        HttpEntity<Collateral> request = new HttpEntity<>(new CarDto(1L,null,null,null,null,null));
        HttpEntity<Collateral> request = new HttpEntity<>(new AirplaneDto(1L,null,null,null,null,null,0,0));
//        CarDto dto=restTemplate.postForObject(resourceURL,request,CarDto.class);
        AirplaneDto dto=restTemplate.postForObject(resourceURL,request,AirplaneDto.class);
        System.out.println(dto);
    }

    @Test
    public void testDto() throws JsonProcessingException {
        String resultJson = new ObjectMapper().writeValueAsString(getTestCarDto());
        System.out.println(resultJson);
        assertTrue(resultJson, resultJson.contains("type"));
        assertTrue(resultJson, resultJson.contains("carValuations"));
        assertTrue(resultJson, resultJson.contains("month"));
    }

    private CarDto getTestCarDto(){
        Set<CarValuationDto> carValuationDtos=new HashSet<>();
        CarValuationDto carValuationDto = new CarValuationDto(1L, LocalDate.now(), new BigDecimal("1000000"), null);
        carValuationDtos.add(carValuationDto);
        return new CarDto(1L,"lada","2110",85D, (short) 2019, carValuationDtos);
    }
    private AirplaneDto getTestAirplaneDto(){
        Set<AirplaneValuationDto> airplaneValuationDtos=new HashSet<>();
        AirplaneDto airplaneDto=new AirplaneDto();
        AirplaneValuationDto airplaneValuationDto = new AirplaneValuationDto();
        airplaneValuationDto.setDate(LocalDate.now());
        airplaneValuationDto.setValue(new BigDecimal("230000001"));
        airplaneValuationDto.setAirplaneDto(airplaneDto);
        airplaneValuationDtos.add(airplaneValuationDto);

        airplaneDto.setBrand("SuperJet");
        airplaneDto.setModel("777");
        airplaneDto.setManufacturer("Sukhoy");
        airplaneDto.setYear((short) 2010);
        airplaneDto.setAirplaneValuations(airplaneValuationDtos);
        airplaneDto.setFuelCapacity(1000);
        airplaneDto.setSeats(50);
        return airplaneDto;
    }


}
